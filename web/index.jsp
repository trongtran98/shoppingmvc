<%-- 
    Document   : index
    Created on : Mar 20, 2018, 4:48:13 PM
    Author     : Trong Tran
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body bgcolor="#FFFFCC">
        <h1>welcome to Shop Stop!</h1>
        <hr/>
        <jsp:useBean id="prod" class="model.ProductCart" scope="session"/>
        <form action="ShoppingCart" name="shoppingForm" method="post">
            <b>Products</b><br/>
            <select name="products">
                <c:forEach items="${prod.products}" var="item">
                    <option>
                        ${item.productId}${"|"}${item.productName}${"|"}${item.productType}${"|"}${item.price}
                    </option>
                </c:forEach>
            </select>
            <br/><br/>
            <b>Quantity </b><input type="text" name="qty" value="1"/><br/>
            <input type="hidden" name="action" value="ADD"/>
            <input type="submit" name="Submit" value="Add to Cart"/>
        </form>
        <p>${message}</p>
        <jsp:include page="cart.jsp" flush="true"/>
    </body>
</html>
