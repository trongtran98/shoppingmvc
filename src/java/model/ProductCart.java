/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Trong Tran
 */
public class ProductCart {

    private final List cartItems = new ArrayList();

    public ProductCart() {

    }
    private float amount;

    private List products;

    public List getProducts() {
        List temp = new ArrayList();
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String dbUser = "sa";
            String dbPassword = "123456";
            String url = "jdbc:sqlserver://localhost;databaseName=ShoppingMVC";
            Connection conn = DriverManager.getConnection(url, dbUser, dbPassword);
            Statement s = conn.createStatement();
            ResultSet rs = s.executeQuery("select * from Products");
            while (rs.next()) {
                Product item = new Product(rs.getInt("productId"), rs.getString("productName"), rs.getString("productType"), rs.getFloat("price"));
                temp.add(item);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return temp;
    }

    public void addItem(int productId, String productName, String productType, float price, int quantity) {
        Product item = null;
        boolean match = false;
        for (int i = 0; i < cartItems.size(); i++) {
            if (((Product) cartItems.get(i)).getProductId() == productId) {
                item = (Product) cartItems.get(i);
                setAmount(getAmount() + quantity * item.getPrice());
                item.setQuantity(item.getQuantity() + quantity);
                match = true;
                break;
            }
        }
        if (!match) {
            item = new Product();
            item.setProductId(productId);
            item.setProductName(productName);
            item.setProductType(productType);
            item.setPrice(price);
            setAmount(getAmount() + quantity * item.getPrice());
            item.setQuantity(quantity);
            cartItems.add(item);
        }
    }

    public void removeItem(int productId) {
        for (int i = 0; i < cartItems.size(); i++) {
            Product item = (Product) cartItems.get(i);
            if (item.getProductId() == productId) {
                setAmount(getAmount() - item.getPrice() * item.getQuantity());
                cartItems.remove(i);
                break;
            }
        }
    }

    public List getCartItems() {
        return cartItems;
    }

    public float getAmount() {
        return this.amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

}
