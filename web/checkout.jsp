<%-- 
    Document   : Checkout
    Created on : Mar 20, 2018, 5:01:56 PM
    Author     : Trong Tran
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="prod" class="model.ProductCart" scope="session"/>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Transaction</title>
    </head>
    <body bgcolor="#FFFFcc">
        <h1>Transaction Details.</h1>
    <center>
        <table>
            <tr>
                <td><b> Product Id </b></td>
                <td><b> Product Name </b></td>
                <td><b> Product Type </b></td>
                <td><b> Price </b></td>
                <td><b> Quantity </b></td>
            </tr>
            <c:forEach items="${prod.cartItems}" var="item">
                <tr>
                    <td>${item.productId}</td>
                    <td>${item.productName}</td>
                    <td>${item.productType}</td>
                    <td>${item.price}</td>
                    <td>${item.quantity}</td>

                </tr>
            </c:forEach>
            <tr>
                <td></td>
                <td></td>
                <td><b> Total </b></td>
                <td><b> ${prod.amount}</b></td>
                <td></td>
            </tr>
        </table>
        <br/>
        <a href="index.jsp">Home</a>
    </center>
</body>
</html>
