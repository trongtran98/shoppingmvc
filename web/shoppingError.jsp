<%-- 
    Document   : ShoppingError
    Created on : Mar 20, 2018, 5:00:36 PM
    Author     : Trong Tran
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Shopping</title>
    </head>
    <body>
        <h1>Error Occurred!</h1>
        ${message}
        <a href="index.jsp">Home</a>
    </body>
</html>
